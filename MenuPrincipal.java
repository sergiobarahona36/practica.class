import javax.swing.JOptionPane;

public class MenuPrincipal{
	boolean instancia=false;
	
	Computadora computadora;
	public MenuPrincipal(){
		menu();
		}
	
	
	public void menu(){
		char opcion;
		do{
			opcion=(JOptionPane.showInputDialog("*********************** Menu principal *******************************\n"
			+"\na. Crear instancia de computadora sin valores."
			+"\nb. Crear instancia de computadora con los valores necesarios para cada uno de sus atributos."
			+"\nc. Modificar la placa de la computadora."
			+"\nd. Modificar el modelo de la computadora."
			+"\ne. Modificar el tamano de la pantalla de la computadora."
			+"\nf. Modificar el estado de la computadora."
			+"\ng. Ver los datos de la computadora."
			+"\nh. Salir."
			)).charAt(0);
			
			switch(opcion){
			
			case 'a':
				if(instancia==true){
				Computadora computadora= new Computadora();
				JOptionPane.showMessageDialog(null,"Guardado con exito");
				}else{
					JOptionPane.showMessageDialog(null, "Debe crear una instancia");
					}
				
			break;
			
			case 'b':
				String modelo="";
				String placa="";
				String estado="";
				int pantalla=0;
				
				
				
				modelo=JOptionPane.showInputDialog("Digite el modelo de la computadora");
				
				placa=JOptionPane.showInputDialog("Digite la placa de la computadora");
				
				estado=JOptionPane.showInputDialog("Digite el estado de la computadora");
				
				pantalla=Integer.parseInt (JOptionPane.showInputDialog("Digite la medida de pantalla de la computadora"));
				
				computadora= new Computadora(modelo,placa,pantalla);
				computadora.setEstado(estado);
				
				instancia=true;
				
			break;
			
			case 'c':
				placa=JOptionPane.showInputDialog ("Digite el la placa");
				computadora.setPlaca(placa);
			break;
			
			case 'd':
				modelo=JOptionPane.showInputDialog ("Digite el modelo");
				computadora.setModelo(modelo);
			break;
			
			case 'e':
				pantalla=Integer.parseInt(JOptionPane.showInputDialog ("Digite el tamano de la pantalla"));
				computadora.setPantalla(pantalla);
			break;
			
			case 'f':
				estado=JOptionPane.showInputDialog ("Digite el estado del equipo");
				computadora.setEstado(estado);
			break;
			
			case 'g': JOptionPane.showMessageDialog(null, "Datos de la computadora :"+"\n"+computadora.toString());
			break;
			
			case 'h':JOptionPane.showMessageDialog(null, "Hasta luego");
			break;
			
			default: {JOptionPane.showMessageDialog(null,"Opcion invalida");}
			
			
				}//Fin del switch
			}while(opcion!='h');
		}
	public static void main (String arg[]){
		MenuPrincipal menuPrincipal = new MenuPrincipal();
	
	}//fin del main
}//fin de la clase MenuPrincipal
